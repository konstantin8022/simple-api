<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Task;
use App\Models\Card;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class CardSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
         DB::statement('SET FOREIGN_KEY_CHECKS=0;');
       Task::truncate();
       Card::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        \App\Models\Card::factory() ->count(10)->create()->each(function($user){
        //factory( Card::class,10)->
         App\Models\Task::factory()-> count(10)->create([
                'card_id' => $card->id,
            ]);
        });
    }
}
