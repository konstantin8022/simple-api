<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DeskList extends Model
{
    use HasFactory;

    protected $fillable =['name','desk_id'];

 protected $primaryKey = 'id';
 protected $table = 'desk_lists';

     public function cards(){
    	return $this->hasMany(Card::class,'list_id','id' );
    }

     public function desk(){
    	return $this->belongsTo(Desk::class,'list_id','id' );
    }
}
