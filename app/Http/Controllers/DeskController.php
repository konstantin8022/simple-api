<?php

namespace App\Http\Controllers;

use App\Models\Desk;
use Illuminate\Http\Request;
use App\Http\Resources\DeskResource;
use App\Http\Requests\StoreDeskRequest;
use Illuminate\Http\Response;
//use Illuminate\Support\Facades\Response;
class DeskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //return Desk::all();
        return DeskResource::collection(Desk::all());
        //return DeskResource::collection(Desk::with('lists')->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreDeskRequest $request)
    {
        $rez=Desk::create($request->validated());
        //$rez=Desk::create($request->all());
        return new DeskResource($rez);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Desk  $desk
     * @return \Illuminate\Http\Response
     */
    //public function show($id)
    public function show(Desk $desk)
    {
       // return Desk::findOrFail($id);
       // return new DeskResource( Desk::findOrFail($id));
        return new DeskResource($desk);
}
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Desk  $desk
     * @return \Illuminate\Http\Response
     */
    public function update(StoreDeskRequest $request, Desk $desk)
    {
        
        $desk->update($request->validated());

        return  new DeskResource($desk); //$desk
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Desk  $desk
     * @return \Illuminate\Http\Response
     */
    public function destroy(Desk $desk)
    {
        $desk->delete();

        //return response()->json(null, Response::HTTP_CREATED);
        return response(null, Response::HTTP_NO_CONTENT );
    }
}
