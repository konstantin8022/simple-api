<?php

namespace App\Http\Controllers;

use App\Models\DeskList;
use Illuminate\Http\Request;
use App\Http\Resources\DeskListResource;
use Illuminate\Http\Response;
use App\Http\Requests\DeskListStoreRequest;
class ListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        //return DeskList::all();
        return DeskListResource::collection(DeskList::with('cards')->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DeskListStoreRequest $request)
    {
    

      $rez=DeskList::create($request->all());
        return new  DeskListResource($rez);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DeskList  $deskList
     * @return \Illuminate\Http\Response
     */
    public function show($id)
   // public function show(DeskList $id)
    {
      //  return DeskList::findOrFail($id);

    return new DeskListResource( DeskList::findOrFail($id));
       // return new DeskListResource($deskList);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DeskList  $deskList
     * @return \Illuminate\Http\Response
     */
    public function update(DeskListStoreRequest $request, $id)
    {
        
        $updaterez=DeskList::find($id);
        $updaterez->update($request->validated());

            return  new DeskListResource($updaterez ); //$desk

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DeskList  $deskList
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delrez=DeskList::find($id);

        $delrez->delete();
        return response(null, Response::HTTP_NO_CONTENT );
    }
}
